package com.flockinger.small1;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import scala.annotation.meta.setter;

@SpringBootApplication
@Configuration()
public class Small1Application {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Small1Application.class);
		app.run(args);
	}
}
