package com.flockinger.small1.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flockinger.small1.DAO.ChatDAO;
import com.flockinger.small1.entity.ChatMessage;
import com.flockinger.small1.entity.ChatRoom;


@Service
public class BasicChatService implements ChatService{

	@Autowired
	private ChatDAO chatDao;

	@Transactional
	public List<ChatMessage> getMessagesFrom(String name, Date from) {
		ChatRoom room = chatDao.getRoom(name);

		return room.getMessages()
					.stream()
					.filter((message) -> isMessageBefore(from, message))
					.sorted()
					.collect(Collectors.toCollection(ArrayList::new));
	}

	private boolean isMessageBefore(Date before, ChatMessage message) {
		boolean res = false;
		DateTime beforeTime = new DateTime(before);

		if (message.getCreated() != null) {
			res = beforeTime.isBefore(message.getCreated().getTime());
		}

		return res;
	}

	@Transactional
	public void addMessage(ChatMessage message) {
		chatDao.addMessage(message);
	}

	@Transactional
	public void setChatDao(ChatDAO chatDao) {
		this.chatDao = chatDao;
	}

}
