package com.flockinger.small1.service;

import java.util.Date;
import java.util.List;

import com.flockinger.small1.DAO.ChatDAO;
import com.flockinger.small1.entity.ChatMessage;


public interface ChatService {
	
	List<ChatMessage> getMessagesFrom(String name, Date from);

	void addMessage(ChatMessage message);

	void setChatDao(ChatDAO chatDao);
}
