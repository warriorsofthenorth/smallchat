package com.flockinger.small1.DAO;

import javax.persistence.EntityManager;

import com.flockinger.small1.entity.ChatMessage;
import com.flockinger.small1.entity.ChatRoom;

public interface ChatDAO{
	
	ChatRoom getRoom(String name);
	
	void addMessage(ChatMessage message);
	
	void deleteRoom(String name);
	
	void setEntityManager(EntityManager entityManager);
}
