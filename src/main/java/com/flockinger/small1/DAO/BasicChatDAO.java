package com.flockinger.small1.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.flockinger.small1.entity.ChatMessage;
import com.flockinger.small1.entity.ChatRoom;

	

@Repository
public class BasicChatDAO implements ChatDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public ChatRoom getRoom(String name)
	{
		ChatRoom room = findRoomByName(name);
		
		if(room == null)
			room = createRoom(name);
		
		return room;
	}
	
	private ChatRoom findRoomByName(String name)
	{
		ChatRoom res = null;
		List<ChatRoom> results = entityManager.createNamedQuery("findRoomByName",ChatRoom.class)
											  .setParameter("name", name)
											  .getResultList();
		if(results != null && results.size() >= 1)
			res = results.get(0);
		
		return res;
	}
	
	private ChatRoom createRoom(String name)
	{
		ChatRoom newRoom = new ChatRoom();
		newRoom.setRoomName(name);
		
		entityManager.persist(newRoom);
		
		return newRoom;
	}
	
	public void deleteRoom(String name)
	{
		ChatRoom room = findRoomByName(name);
		
		if(room != null)
			entityManager.remove(room);
	}
	
	public void addMessage(ChatMessage message)
	{
		ChatRoom room = getRoom(message.getRoomName());
		
		List<ChatMessage> messages = room.getMessages();
		
		if(messages == null)
		{
			messages = new ArrayList<>();
			room.setMessages(messages);
		}
		
		messages.add(message);
	}
	
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
