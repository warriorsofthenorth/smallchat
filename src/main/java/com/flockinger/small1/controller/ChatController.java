package com.flockinger.small1.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flockinger.small1.entity.ChatMessage;
import com.flockinger.small1.service.ChatService;

@RestController
@RequestMapping("chat")
public class ChatController {
	
	@Autowired
	private ChatService chatService;
	
	
	@RequestMapping(method=RequestMethod.GET)
	public List<ChatMessage> getChat(@RequestParam(name="name")String name, @RequestParam(name="from") @DateTimeFormat(pattern="yyyy-MM-ddHH:mm") Date from)
	{
		return chatService.getMessagesFrom(name, from);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public void addMessage(@RequestBody ChatMessage message)
	{
		chatService.addMessage(message);
	}
	
	public void setChatService(ChatService chatService) {
		this.chatService = chatService;
	}
}
