package com.flockinger.small1.entity;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ChatMessage implements Comparable<ChatMessage>{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonIgnore
	private long id;
	
	@CreationTimestamp
	private Date created;
	
	@NotNull
	@Length(min=3,max=30)
	private String user;
	
	@NotNull
	@Length(min=1,max=255)
	private String message;
	
	@NotNull
	private String roomName;
	
	
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
	@Override
	public int compareTo(ChatMessage o) {
		int res = 1;
		
		if(o != null && o.created != null)
			res = created.compareTo(o.getCreated());
		
		return res;
	}
	
}
