package com.flockinger.small1.DAO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.flockinger.small1.entity.ChatMessage;
import com.flockinger.small1.entity.ChatRoom;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/dbContext.xml", 
		                           "file:src/test/resources/beanContext.xml"})
@TestPropertySource(locations="file:src/test/resources/testApp.properties")
public class ChatDAOTest {

	
	@Autowired
	private ChatDAO chatDao;
	
	private ChatMessage validMessage;
	private final static String roomName = "test room";
	
	@Before
	public void before()
	{
		validMessage = new ChatMessage();
		validMessage.setMessage("test message");
		validMessage.setUser("testUser");
		validMessage.setRoomName(roomName);
	}
	
	
	@Test
	@Transactional
	public void getRoom_withExistingRoom_shouldFindCreatedRoomTest()
	{
		chatDao.getRoom(roomName);
		
		ChatRoom foundRoom = chatDao.getRoom(roomName);
		
		assertNotNull(foundRoom);
		
		chatDao.deleteRoom(roomName);
	}
	
	@Test
	@Transactional
	public void getRoom_withNotExistingRoom_shouldReturnNewRoomTest()
	{
		ChatRoom foundRoom = chatDao.getRoom(roomName);
		
		assertNotNull(foundRoom);
		
		chatDao.deleteRoom(roomName);
	}
	
	@Test
	@Transactional
	public void addMessage_withValidMessage_ShouldWorkTest()
	{
		chatDao.getRoom(roomName);
		chatDao.addMessage(validMessage);
		
		List<ChatMessage> messages = chatDao.getRoom(roomName).getMessages();
		
		assertNotNull(messages);
		assertEquals(messages.size(), 1);
		
		ChatMessage resultMessage = messages.get(0);
		
		assertEquals(resultMessage.getMessage(), validMessage.getMessage());
		assertEquals(resultMessage.getRoomName(), validMessage.getRoomName());
		assertEquals(resultMessage.getUser(), validMessage.getUser());
		
		chatDao.deleteRoom(roomName);
	}
	
	@Test
	@Transactional
	public void deleteRoom_withExistingRoom_shouldWorkTest()
	{
		ChatRoom newRoom = chatDao.getRoom(roomName);
		newRoom.setMessages(Arrays.asList(new ChatMessage[]{validMessage}));
		
		chatDao.deleteRoom(roomName);
		
	}
}
